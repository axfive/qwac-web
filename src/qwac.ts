import {IndexedResourceManager} from '/resource_manager.js';
import * as audio from '/qwac/audio.js';

interface CartridgeExports {
    memory: WebAssembly.Memory;
    qwac_game_create(timestamp: number): number;
    qwac_game_event_gamepad(game: number, player: number, button: number, pressed: number): void;
    qwac_game_update(game: number, delta: number): void;
    qwac_game_destroy(game: number): void;
}

const enum QwacGamepadButton {
    Up = 0,
    Down,
    Left,
    Right,
    Select,
    Start,
    LeftShoulder,
    RightShoulder,
    North,
    South,
    East,
    West,
}

export class Console {
    private _last_timestamp: DOMHighResTimeStamp | undefined = undefined;

    private _cartridge = undefined as unknown as WebAssembly.Instance;
    private exports = undefined as unknown as CartridgeExports;
    private game = undefined as unknown as number;
    private audio_nodes = new IndexedResourceManager<audio.AudioSlot>();
    private canvases = new IndexedResourceManager<CanvasRenderingContext2D>();

    private textures = new IndexedResourceManager<ImageBitmap>();

    private audio_context_current_time: number;

    /** Bound listeners, used for removing event listeners.
     */
    private listeners: {
        keydown: any,
        keyup: any,
    };

    /** Construct enough of the object to initialize the WebAssembly module.
     */
    private constructor(
        private screen: HTMLCanvasElement,
        private audio_context: AudioContext,
    ) {
        // Add the screen canvas as canvas 0.
        this.canvas_create(screen);
        this.audio_context_current_time = audio_context.currentTime;

        this.listeners = {
            keydown: this.keydown.bind(this),
            keyup: this.keyup.bind(this),
        };


    }

    /** Finish initializing the object for use.
     *
     * This also sets up relevant event listeners.
     */
    private finish(
        cartridge: WebAssembly.Instance,
    ): Console {
        this._cartridge = cartridge;
        this.exports = cartridge.exports as unknown as CartridgeExports;
        this.game = this.exports.qwac_game_create(Date.now());

        if ('qwac_game_event_gamepad' in this.exports) {
            this.screen.addEventListener('keydown', this.listeners.keydown, {capture: true});
            this.screen.addEventListener('keyup', this.listeners.keyup, {capture: true});
        }

        return this;
    }

    private keyevent(event: KeyboardEvent, pressed: boolean): void {
        if (event.key === 'Escape') {
            // Allow the escape key to propagate
            return;
        }

        event.preventDefault();
        event.stopPropagation();
        if (!event.repeat) {
            let button: QwacGamepadButton | undefined = undefined;
            switch (event.key) {
                case 'ArrowUp':
                    button = QwacGamepadButton.Up;
                    break;

                case 'ArrowDown':
                    button = QwacGamepadButton.Down;
                    break;

                case 'ArrowLeft':
                    button = QwacGamepadButton.Left;
                    break;

                case 'ArrowRight':
                    button = QwacGamepadButton.Right;
                    break;

                case 'Z':
                case 'z':
                    button = QwacGamepadButton.South;
                    break;

                case 'X':
                case 'x':
                    button = QwacGamepadButton.East;
                    break;

                case 'Enter':
                    button = QwacGamepadButton.Start;
                    break;
            }

            if (button !== undefined) {
                this.exports.qwac_game_event_gamepad(this.game, 0, button, pressed ? 1 : 0);
            }
        }
    }

    private keydown(event: KeyboardEvent): void {
        this.keyevent(event, true);
    }

    private keyup(event: KeyboardEvent): void {
        this.keyevent(event, false);
    }

    private canvas_create(canvas?: HTMLCanvasElement): number {
        if (canvas === undefined) {
            canvas = document.createElement('canvas');
        }

        canvas.width = 256;
        canvas.height = 256;

        // Ensure alpha is enabled and imageSmoothing is disabled.
        const rendering_context = canvas.getContext("2d", {
            alpha: true,
        })!;
        rendering_context.imageSmoothingEnabled = false;

        return this.canvases.add(rendering_context);
    }

    private canvas_resize(canvas_id: number, width: number, height: number): void {
        const canvas = this.canvases.get(canvas_id);
        canvas.canvas.width = width;
        canvas.canvas.height = height;
    }

    private canvas_clear(canvas_id: number): void {
        const canvas = this.canvases.get(canvas_id);
        canvas.clearRect(0, 0, canvas.canvas.width, canvas.canvas.height);
    }

    private canvas_draw_rectangle(canvas_id: number, x: number, y: number, width: number, height: number, r: number, g: number, b: number, a: number): void {
        const canvas = this.canvases.get(canvas_id);
        canvas.fillStyle = `rgba(${r}, ${g}, ${b}, ${a})`;
        canvas.fillRect(x, y, width, height);
    }

    private canvas_draw_texture(canvas_id: number, texture_id: number, source_x: number, source_y: number, source_width: number, source_height: number, dest_x: number, dest_y: number, dest_width: number, dest_height: number): void {
        const canvas = this.canvases.get(canvas_id);
        const texture = this.textures.get(texture_id);
        canvas.drawImage(
            texture,
            source_x,
            source_y,
            source_width,
            source_height,
            dest_x,
            dest_y,
            dest_width,
            dest_height,
        );
    }

    private canvas_delete(canvas_id: number): void {
        this.canvases.remove(canvas_id);
    }

    private async texture_from_canvas(canvas_id: number): Promise<number> {
        const canvas = this.canvases.get(canvas_id).canvas;
        return this.textures.add(await createImageBitmap(canvas));
    }

    private async texture_from_rgba(pointer: number, width: number, height: number): Promise<number> {
        const array = new Uint8ClampedArray(this.exports.memory.buffer, pointer, width * height * 4);
        const image_data = new ImageData(array, width, height);
        return this.textures.add(await createImageBitmap(image_data));
    }

    private async texture_from_png(pointer: number, size: number): Promise<number> {
        const array = new Uint8Array(this.exports.memory.buffer, pointer, size);
        const blob = new Blob([array], {'type': 'image/png'});
        return this.textures.add(await createImageBitmap(blob));
    }

    private async texture_from_webp(pointer: number, size: number): Promise<number> {
        const array = new Uint8Array(this.exports.memory.buffer, pointer, size);
        const blob = new Blob([array], {'type': 'image/webp'});
        return this.textures.add(await createImageBitmap(blob));
    }

    private texture_delete(texture_id: number): void {
        this.textures.remove(texture_id);
    }

    private audio_oscillator(oscillator_type: number): number {
        let type: keyof typeof audio.OscillatorType;
        switch (oscillator_type) {
            case audio.OscillatorType.sine: {
                type = 'sine';
                break;
            }
            case audio.OscillatorType.square: {
                type = 'square';
                break;
            }
            case audio.OscillatorType.sawtooth: {
                type = 'sawtooth';
                break;
            }
            case audio.OscillatorType.triangle: {
                type = 'triangle';
                break;
            }
            default: {
                throw new RangeError('Illegal OscillatorType specified');
            }
        }
        return this.audio_nodes.add(new audio.OscillatorSlot(type));
    }

    private audio_source_delay(slot: number, seconds: number) {
        const node = this.audio_nodes.get(slot) as unknown as audio.AudioSourceSlot;
        node.delay = seconds;
    }

    private audio_source_length(slot: number, seconds: number) {
        const node = this.audio_nodes.get(slot) as unknown as audio.AudioSourceSlot;
        node.length = seconds;
    }

    private audio_source_play(slot: number): void {
        const node = this.audio_nodes.get(slot) as audio.OscillatorSlot;
        node.play(this.audio_context);
    }

    private audio_source_stop(slot: number): void {
        const node = this.audio_nodes.get(slot) as audio.OscillatorSlot;
        node.stop();
    }

    private audio_gain(): number {
        return this.audio_nodes.add(new audio.GainSlot());
    }

    private audio_delay(max_seconds: number): number {
        return this.audio_nodes.add(new audio.DelaySlot(max_seconds));
    }

    private audio_set_value(slot: number, value: number): void {
        const node = this.audio_nodes.get(slot) as audio.SingleValueSlot;
        node.params.push(new audio.RelativeParamSet(value));
    }

    private audio_set_value_at_time(slot: number, time: number, value: number): void {
        const node = this.audio_nodes.get(slot) as audio.SingleValueSlot;
        node.params.push(new audio.RelativeParamTimeSet(time, value));
    }

    private audio_linear_ramp_to_value_at_time(slot: number, time: number, value: number): void {
        const node = this.audio_nodes.get(slot) as audio.SingleValueSlot;
        node.params.push(new audio.RelativeParamLinearRamp(time, value));
    }

    private audio_exponential_ramp_to_value_at_time(slot: number, time: number, value: number): void {
        const node = this.audio_nodes.get(slot) as audio.SingleValueSlot;
        node.params.push(new audio.RelativeParamExponentialRamp(time, value));
    }

    private audio_connect(source_id: number, target_id: number): void {
        const source = this.audio_nodes.get(source_id) as audio.AudioSlot;
        const target = this.audio_nodes.get(target_id) as audio.AudioSlot;
        source.downstream.add(target);
        target.upstream.add(source);
    }

    private audio_disconnect(source_id: number, target_id: number): void {
        const source = this.audio_nodes.get(source_id) as audio.AudioSlot;
        const target = this.audio_nodes.get(target_id) as audio.AudioSlot;
        source.downstream.delete(target);
        target.upstream.delete(source);
    }

    private audio_sink(slot: number): void {
        const node = this.audio_nodes.get(slot) as audio.AudioSlot;
        node.downstream.add(audio.SINK);
    }

    private audio_unsink(slot: number): void {
        const node = this.audio_nodes.get(slot) as audio.AudioSlot;
        node.downstream.delete(audio.SINK);
    }

    private audio_delete(slot: number): void {
        const node = this.audio_nodes.remove(slot);
        for (let upstream of node.upstream) {
            upstream.downstream.delete(node);
        }
        for (let downstream of node.downstream) {
            if (downstream instanceof audio.AudioSlot) {
                downstream.upstream.delete(node);
            }
        }
    }


    private log_message(pointer: number, size: number) {
        const utf8decoder = new TextDecoder();
        const array = new Uint8Array(this.exports.memory.buffer, pointer, size);
        console.log(utf8decoder.decode(array));
    }

    /** Create the console.
     *
     * Uses a factory in order to allow async to work.
     */
    public static async create(
        data: ArrayBuffer,
        canvas: HTMLCanvasElement,
        audio_context: AudioContext,
    ): Promise<Console> {
        let console = new Console(canvas, audio_context);

        const import_object: WebAssembly.Imports = {
            env: {
                qwac_log: console.log_message.bind(console),
                qwac_canvas_create: console.canvas_create.bind(console),
                qwac_canvas_resize: console.canvas_resize.bind(console),
                qwac_canvas_clear: console.canvas_clear.bind(console),
                qwac_canvas_draw_rectangle: console.canvas_draw_rectangle.bind(console),
                qwac_canvas_draw_texture: console.canvas_draw_texture.bind(console),
                qwac_canvas_delete: console.canvas_delete.bind(console),
                qwac_texture_from_canvas: console.texture_from_canvas.bind(console),
                qwac_texture_from_rgba: console.texture_from_rgba.bind(console),
                qwac_texture_from_png: console.texture_from_png.bind(console),
                qwac_texture_from_webp: console.texture_from_webp.bind(console),
                qwac_texture_delete: console.texture_delete.bind(console),
                qwac_audio_oscillator: console.audio_oscillator.bind(console),
                qwac_audio_source_delay: console.audio_source_delay.bind(console),
                qwac_audio_source_length: console.audio_source_length.bind(console),
                qwac_audio_source_play: console.audio_source_play.bind(console),
                qwac_audio_source_stop: console.audio_source_stop.bind(console),
                qwac_audio_gain: console.audio_gain.bind(console),
                qwac_audio_delay: console.audio_delay.bind(console),
                qwac_audio_set_value: console.audio_set_value.bind(console),
                qwac_audio_set_value_at_time: console.audio_set_value_at_time.bind(console),
                qwac_audio_linear_ramp_to_value_at_time: console.audio_linear_ramp_to_value_at_time.bind(console),
                qwac_audio_exponential_ramp_to_value_at_time: console.audio_exponential_ramp_to_value_at_time.bind(console),
                qwac_audio_connect: console.audio_connect.bind(console),
                qwac_audio_disconnect: console.audio_disconnect.bind(console),
                qwac_audio_sink: console.audio_sink.bind(console),
                qwac_audio_unsink: console.audio_unsink.bind(console),
                qwac_audio_delete: console.audio_delete.bind(console),
            }
        };
        let result = await WebAssembly.instantiate(data, import_object);

        return console.finish(result.instance);
    }

    public async update(timestamp: DOMHighResTimeStamp): Promise<void> {
        if (this._last_timestamp !== undefined) {
            const diff = timestamp - this._last_timestamp;
            this.exports.qwac_game_update(this.game, diff);
        }
        this.audio_context_current_time = this.audio_context.currentTime;

        this._last_timestamp = timestamp;
    }

    public shutdown(): void {
        this.screen.removeEventListener('keydown', this.listeners.keydown, {capture: true});
        this.screen.removeEventListener('keyup', this.listeners.keyup, {capture: true});
        this.exports.qwac_game_destroy(this.game);
    }
}

