/** A simple index-based resource manager, for managing shared resources that
 * can be used from WASM.
 */
export class IndexedResourceManager<Resource> implements Iterable<Resource> {
    private resources: Array<Resource | null> = [];
    private empty_slots: Array<number> = [];

    /** Add a resource into the manager, and return its slot.
     *
     * Always sets the lowest-available slot.
     */
    public add(resource: Resource): number {
        if (this.empty_slots.length > 0) {
            const index = this.empty_slots[0];
            this.empty_slots.splice(0, 1);
            this.resources[index] = resource;
            return index;
        } else {
            this.resources.push(resource);
            return this.resources.length - 1;
        }
    }
    
    public get(index: number): Resource {
        if (index >= this.resources.length) {
            throw new RangeError('Tried to access a resource out of bounds');
        }
        const resource = this.resources[index];
        if (resource === null) {
            throw new Error('Tried to access a deleted resource');
        }
        return resource;
    }

    public remove(index: number): Resource {
        const resource = this.get(index);
        const resources = this.resources;
        const empty_slots = this.empty_slots;

        // If this resource is at the end, simply shrink the array.
        if (resources.length - 1 == index) {
            resources.length = index;

            // Also shrink down as many empty slots as possible.
            while (resources.length > 0 && resources[resources.length - 1] === null) {
                resources.pop();

                // This index should be the last index in empty_slots as well
                empty_slots.pop();
            }
        } else {
            resources[index] = null;
            empty_slots.push(index);
            empty_slots.sort(function (a, b) { return a - b });

            // We didn't remove the last index in resources, so we don't have to
            // worry about popping all the nulls off the end.
        }
        return resource;
    }

    public [Symbol.iterator](): Iterator<Resource> {
        let slot = 0;
        return {
            next: () => {
                while (slot < this.resources.length && this.resources[slot] === null) {
                    ++slot;
                }
                const done = slot >= this.resources.length;
                if (done) {
                    return {
                        done: true,
                    } as IteratorResult<Resource>;
                }

                const resource = this.resources[slot] as Resource;
                ++slot;
                return {
                    done: false,
                    value: resource,
                }
            }
        }
    }
}
